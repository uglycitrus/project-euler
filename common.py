import math

def prop_divisors(x):
    """
    >>> l = prop_divisors(220)
    >>> l.sort()
    >>> l
    [1, 2, 4, 5, 10, 11, 20, 22, 44, 55, 110]
    >>> l = prop_divisors(284)
    >>> l.sort()
    >>> l
    [1, 2, 4, 71, 142]
    """
    if x < 0:
        x = x - 2 * x
    divisors = [1]
    max_divisor = x / 2 + 1
    i = 2
    while i < max_divisor:
        if x % i == 0:
            max_divisor = x / i
            divisors.extend((i, max_divisor))
        i += 1
    return divisors


def is_prime(x):
    return len(prop_divisors(x)) == 1


def rotate(num):
    """
    get all unique rotations of a number
    >>> rotate(11)
    set([])
    >>> rotate(9009)
    set([9900, 990, 99])
    """
    rtrn = set()
    original = num
    digits = int(math.log(num, 10))
    for i in range(digits):
        last = num % 10
        num /= 10
        num += last * int(math.pow(10, digits))
        if num != original:
            rtrn = rtrn | set([num,])
    return rtrn

def list_primes(max):
    for i in xrange(2, max):
        if is_prime(i):
            yield i

def is_prime(num):
    for i in xrange(2, int(math.sqrt(num)) + 1):
        if num % i == 0:
            return False
    return True

def is_palendrome(s):
    return s == s[::-1]
