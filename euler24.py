"""
A permutation is an ordered arrangement of objects. For example, 3124 is one possible permutation of the digits 1, 2, 3 and 4. If all of the permutations are listed numerically or alphabetically, we call it lexicographic order. The lexicographic permutations of 0, 1 and 2 are:

012   021   102   120   201   210

What is the millionth lexicographic permutation of the digits 0, 1, 2, 3, 4, 5, 6, 7, 8 and 9?
"""
import math

def brute_force():
    lex_count = 1
    counter = 123456789
    inc = 9
    while lex_count < 1000000:
        counter += inc
        counter_str = str(counter)
        if (
                ('0' in counter_str or
                    len(counter_str) == 9) and
                '1' in counter_str and
                '2' in counter_str and
                '3' in counter_str and
                '4' in counter_str and
                '5' in counter_str and
                '6' in counter_str and
                '7' in counter_str and
                '8' in counter_str and
                '9' in counter_str):
            lex_count += 1
    print lex_count, counter

def grace():
    counter = 1000000
    position = 9
    choices = range(0,10)
    solution = ''
    while counter > 0:
        for i in range(10):
            test = i * math.factorial(position)
            if test > counter:
                counter -= (i - 1) * math.factorial(position)
                solution += str(choices.pop(i - 1))
                position -= 1
                break
    while choices:
        solution += str(choices.pop(0))
    return solution


