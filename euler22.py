"""
PROBLEM #22:

Using names.txt (right click and 'Save Link/Target As...'), a 46K text file containing over five-thousand first names, begin by sorting it into alphabetical order. Then working out the alphabetical value for each name, multiply this value by its alphabetical position in the list to obtain a name score.

For example, when the list is sorted into alphabetical order, COLIN, which is worth 3 + 15 + 12 + 9 + 14 = 53, is the 938th name in the list. So, COLIN would obtain a score of 938  53 = 49714.

What is the total of all the name scores in the file?
"""
import os
import doctest

def new_ord(letter):
    return ord(letter) - 64

def alph_worth(word):
    """
    >>> alph_worth('COLIN')
    53
    """
    worths = map(new_ord, word)
    return sum(worths)

total = 0
line_number = 0
with open(os.getcwd() + '/names.txt') as f:
    for name in sorted(f):
        line_number += 1
        total += alph_worth(name.strip()) * line_number
print total
    
doctest.testmod()
