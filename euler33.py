"""
The fraction 49/98 is a curious fraction, as an inexperienced mathematician in attempting to simplify it may incorrectly believe that 49/98 = 4/8, which is correct, is obtained by cancelling the 9s.

We shall consider fractions like, 30/50 = 3/5, to be trivial examples.

There are exactly four non-trivial examples of this type of fraction, less than one in value, and containing two digits in the numerator and denominator.

If the product of these four fractions is given in its lowest common terms, find the value of the denominator.
"""
def gcd(a, b):
    if a > b:
        x = a
        y = b
    else:
        x = b
        y = a

    while (x % y != 0):
        temp = x
        x = y
        y = temp % x
    return y;

def no33():
    num_prod = 1
    den_prod = 1
    for numerator in xrange(10, 100):
        num_tens = numerator / 10
        num_ones = numerator % 10
        for denominator in xrange(1, 10):
            test = float(numerator) / (num_ones * 10 + denominator)
            if (test < 1.0 and test == float(num_tens) / denominator):
                num_prod *= numerator
                den_prod *= num_ones * 10 + denominator
    return den_prod / gcd(num_prod, den_prod)
                
print no33()
