"""
The number, 197, is called a circular prime because all rotations of the digits: 197, 971, and 719, are themselves prime.

There are thirteen such primes below 100: 2, 3, 5, 7, 11, 13, 17, 31, 37, 71, 73, 79, and 97.

How many circular primes are there below one million?
"""
from common import is_prime, list_primes, rotate

def no35(max):
    count = 0
    gen = list_primes(max)
    found = {i: False for i in xrange(max)}
    for i in gen:
        if found[i]:
            continue
        rotation = rotate(i)
        if not rotation or (
                rotation and all(map(is_prime, rotation))):
            found[i] = True
            count += 1
            print i
            for i in rotation:
                found[i] = True
                count += 1
                print i
    return count

print no35(1000000)
