"""
PROBLEM #28:

Starting with the number 1 and moving to the right in a clockwise direction a 5 by 5 spiral is formed as follows:

21 22 23 24 25
20  7  8  9 10
19  6  1  2 11
18  5  4  3 12
17 16 15 14 13

It can be verified that the sum of the numbers on the diagonals is 101.

What is the sum of the numbers on the diagonals in a 1001 by 1001 spiral formed in the same way?
"""

def square_perimeters():
    length = 0
    while True:
        if length == 0:
            length = 1
            yield 1
        else:
            length += 2
            yield 4 * (length - 1)


def square(x):
    layers = []
    num = 1
    perim = square_perimeters()
    for i in range(x):
        layer = []
        for i in range(next(perim)):
            layer.append(num)
            num += 1
        layers.append(layer)
    return layers
            

def square_sum(square):
    total = 1
    for layer in square[1:]:
        spacing = len(layer)/4
        initial = spacing - 1
        total += layer[initial]
        total += sum([layer[i * spacing - 1] for i in range(2,5)])
    return total

s = square(501) # a 1001x1001 square will have 501 "layers"
print square_sum(s)
