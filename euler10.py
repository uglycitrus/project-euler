"""
The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.
Find the sum of all the primes below two million.
"""

from common import list_primes

def no10(max):
    total = 0
    gen = list_primes(max)
    for i in gen:
        total += i
    return total

print no10(2000000)
