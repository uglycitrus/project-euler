"""
PROBLEM #25
The Fibonacci sequence is defined by the recurrence relation:

Fn = Fn1 + Fn2, where F1 = 1 and F2 = 1.
Hence the first 12 terms will be:

F1 = 1
F2 = 1
F3 = 2
F4 = 3
F5 = 5
F6 = 8
F7 = 13
F8 = 21
F9 = 34
F10 = 55
F11 = 89
F12 = 144
The 12th term, F12, is the first term to contain three digits.

What is the first term in the Fibonacci sequence to contain 1000 digits?
"""
def fib():
    a=0
    b=0
    if a == 0:
        a = 1
        yield 1
    if b == 0:
        b = 1
        yield 1
    while True:
        b, a = a, a + b
        yield a

a = fib()
b = 0
counter = 0
while b < 10**999:
    counter += 1
    b = next(a)
print counter
