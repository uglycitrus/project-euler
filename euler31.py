"""
In England the currency is made up of pound, L, and pence, p, and there are eight coins in general circulation:

1p, 2p, 5p, 10p, 20p, 50p, L1 (100p) and L2 (200p).
It is possible to make L2 in the following way:

1L1 + 150p + 220p + 15p + 12p + 31p
How many different ways can L2 be made using any number of coins?
"""
def no31():
    total = 0
    for a in range(2):
        remaining = 200 - 200 * a
        for b in range(remaining / 100 + 1):
            remaining = 200 - 200 * a - 100 * b
            for c in range(remaining / 50 + 1):
                remaining = 200 - 200 * a - 100 * b - 50 * c
                for d in range(remaining / 20 + 1):
                    remaining = 200 - 200 * a - 100 * b - 50 * c - 20 * d
                    for e in range(remaining / 10 + 1):
                        remaining = 200 - 200 * a - 100 * b - 50 * c - 20 * d - 10 * e
                        for f in range(remaining / 5 + 1):
                            remaining = 200 - 200 * a - 100 * b - 50 * c - 20 * d - 10 * e - 5 * f
                            for g in range(remaining / 2 + 1):
                                remaining = 200 - 200 * a - 100 * b - 50 * c - 20 * d - 10 * e - 5 * f - 2 * g
                                for h in range(remaining + 1):
                                    if 200 * a + 100 * b + 50 * c + 20 * d + 10 * e + 5 * f + 2 * g + h == 200:
                                        total += 1
    return total
