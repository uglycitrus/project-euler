"""
PROBLEM #23:
A perfect number is a number for which the sum of its proper divisors is exactly equal to the number. For example, the sum of the proper divisors of 28 would be 1 + 2 + 4 + 7 + 14 = 28, which means that 28 is a perfect number.

A number n is called deficient if the sum of its proper divisors is less than n and it is called abundant if this sum exceeds n.

As 12 is the smallest abundant number, 1 + 2 + 3 + 4 + 6 = 16, the smallest number that can be written as the sum of two abundant numbers is 24. By mathematical analysis, it can be shown that all integers greater than 28123 can be written as the sum of two abundant numbers. However, this upper limit cannot be reduced any further by analysis even though it is known that the greatest number that cannot be expressed as the sum of two abundant numbers is less than this limit.

Find the sum of all the positive integers which cannot be written as the sum of two abundant numbers.
"""

def prop_divisors(x):
    """
    >>> l = prop_divisors(220)
    >>> l.sort()
    >>> l
    [1, 2, 4, 5, 10, 11, 20, 22, 44, 55, 110]
    >>> l = prop_divisors(284)
    >>> l.sort()
    >>> l
    [1, 2, 4, 71, 142]
    """
    divisors = [1]
    max_divisor = x / 2 + 1
    i = 2
    while i < max_divisor:
        if x % i == 0:
            max_divisor = x / i
            if i == max_divisor:
                divisors.extend((i, ))
            else:
                divisors.extend((i, max_divisor))
        i += 1
    return divisors

def abundants():
    for x in range(1, 28123):
        if sum(prop_divisors(x)) > x:
            yield x

ab_sums = []
ab = [i for i in abundants()]
for i in ab:
    for j in range(len(ab)):
        ab_sums.append(i+j)
print sum(set(range(1,28123)) - set(ab_sums))
