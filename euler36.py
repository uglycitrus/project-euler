"""
The decimal number, 585 = 10010010012 (binary), is palindromic in both bases.

Find the sum of all numbers, less than one million, which are palindromic in base 10 and base 2.

(Please note that the palindromic number, in either base, may not include leading zeros.)
"""
from common import is_palendrome

def no36(max):
    total = 0
    for i in xrange(max):
        if is_palendrome(str(i)) and is_palendrome(bin(i).split('b')[1]):
            print i
            total += i
    return total

print no36(1000000)

