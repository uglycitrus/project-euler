"""
PROBLEM #21:

Let d(n) be defined as the sum of proper divisors of n (numbers less than n which divide evenly into n).
If d(a) = b and d(b) = a, where a  b, then a and b are an amicable pair and each of a and b are called amicable numbers.

For example, the proper divisors of 220 are 1, 2, 4, 5, 10, 11, 20, 22, 44, 55 and 110; therefore d(220) = 284. The proper divisors of 284 are 1, 2, 4, 71 and 142; so d(284) = 220.

Evaluate the sum of all the amicable numbers under 10000.
"""
def prop_divisors(x):
    """
    >>> l = prop_divisors(220)
    >>> l.sort()
    >>> l
    [1, 2, 4, 5, 10, 11, 20, 22, 44, 55, 110]
    >>> l = prop_divisors(284)
    >>> l.sort()
    >>> l
    [1, 2, 4, 71, 142]
    """
    divisors = [1]
    max_divisor = x / 2 + 1
    i = 2
    while i < max_divisor:
        if x % i == 0:
            max_divisor = x / i
            divisors.extend((i, max_divisor))
        i += 1
    return divisors

def is_amicable(a):
    """
    >>> is_amicable(220)
    (220, 284)
    >>> is_amicable(221)
    False
    >>> is_amicable(6)
    False
    """
    b = sum(prop_divisors(a))
    if a == b:
        return False
    elif a == sum(prop_divisors(b)):
        return (a, b)
    else:
        return False

import doctest
doctest.testmod()

amicables = []
for i in range(1,10001):
    amicables.extend(is_amicable(i) or (0, 0))
print sum(set(amicables))
