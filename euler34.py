"""
145 is a curious number, as 1! + 4! + 5! = 1 + 24 + 120 = 145.

Find the sum of all numbers which are equal to the sum of the factorial of their digits.

Note: as 1! = 1 and 2! = 2 are not sums they are not included.
"""
import math

facts = [math.factorial(i) for i in range(10)]

def no34():
    curious=[]
    for target in xrange(3, 2540160):
        total = 0
        target_copy = target
        while (target > 0):
            total += facts[target % 10]
            target /= 10
            if total > target_copy:
                break
        if total == target_copy:
            curious.append(total)
    return sum(curious)

print no34()
