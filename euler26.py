"""
PROBLEM#26:

A unit fraction contains 1 in the numerator. The decimal representation of the unit fractions with denominators 2 to 10 are given:

1/2 =   0.5
1/3 =   0.(3)
1/4 =   0.25
1/5 =   0.2
1/6 =   0.1(6)
1/7 =   0.(142857)
1/8 =   0.125
1/9 =   0.(1)
1/10    =   0.1
Where 0.1(6) means 0.166666..., and has a 1-digit recurring cycle. It can be seen that 1/7 has a 6-digit recurring cycle.

Find the value of d < 1000 for which 1/d contains the longest recurring cycle in its decimal fraction part.
"""
def unit_fraction(x):
    base_equation = 1
    result = 0
    places = 0
    while places < 2000:
        base_equation = (base_equation - (result * x)) * 10
        result = base_equation / x
        yield str(result)


def get_repeat(x):
    if len(str(1.0/x)) < 14:
        return False # truncated not repeating
    else:
        all_nums = ''
        for num in unit_fraction(x):
            all_nums += num
            if len(all_nums) > 14:
                for test in [all_nums[i:] for i in range(len(all_nums))]:
                    initial = all_nums.find(test)
                    test_len = len(test)
                    try:
                        if (test == all_nums[initial + test_len: initial + 2*test_len] and
                                test == all_nums[initial + test_len: initial + 3*test_len]):
                            if len(set(test)) == 1:
                                return test[0]
                            else:
                                return test
                    except IndexError:
                        pass


def no26(x):
    max_count = 0
    max_unit_fraction = 0
    for i in range(2, x):
        repeat = get_repeat(i)
        if repeat:
            if len(repeat) > max_count:
                max_count = len(repeat)
                max_unit_fraction = i
    return max_unit_fraction
            
"""
no26 is my brute force approach. the tricky stuff below, i found online
"""


def recurring_cycle(n, d):
    # solve 10^s % d == 10^(s+t) % d
    # where t is length and s is start
    for t in range(1, d):
        if 1 == 10**t % d:
            return t
    return 0

longest = max(recurring_cycle(1, i) for i in range(2,1001))
print [i for i in range(2,1001) if recurring_cycle(1, i) == longest][0]
